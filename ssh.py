from paramiko import SSHClient, AutoAddPolicy
from socket import create_connection, timeout
import time
import logging


#loggin_level = logging.INFO
#logging.basicConfig(level=loggin_level)
#logger = logging.getLogger('rebooter')
#handler = logging.FileHandler('rebooter.log')
#handler.setLevel(loggin_level)
#formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
#handler.setFormatter(formatter)
#logger.addHandler(handler)


def switch_reachable(switch_ip):
    try:
        create_connection((switch_ip, 22), timeout=1)
    except timeout:
        print(switch_ip + ' unreachable')
        return False
    else:
        print(switch_ip + ' reachable')
        return True


all_swithces = ['x.x.x.x',
                'y.y.y.y']

for switch in all_swithces:
    if switch_reachable(switch):
        client = SSHClient()
        client.set_missing_host_key_policy(AutoAddPolicy())
        client.connect(hostname=switch,
                       username='login',
                       password='password',
                       look_for_keys=False,
                       allow_agent=False)

        with client.invoke_shell() as ssh:
            ssh.send('show ver\n\n\n')
            time.sleep(3)
            print(ssh.recv(10000).decode('utf-8'))
            #ssh.send('/admin reboot \n')
            #time.sleep(1)
            #output = ssh.recv(2000).decode('utf-8')
            #if '(y/n)' in output:
             #   ssh.send('y\n')
              #  time.sleep(1)
    else:
        print('Switch unreachable. Skipp reboot.')
